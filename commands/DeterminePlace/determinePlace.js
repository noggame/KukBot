const Commando = require('discord.js-commando');

// for Image
const images = require('images');

// Map list
var place_a = ['Georgopol_North', 'Georgopol_South', 'School', 'Pochinki', 'Yasnaya Polyana'
            , 'Hospital', 'Mylta', 'Sosnovka Military Base'];
var place_b = ['Shooting Range', 'Zharki', 'Rozhok', 'Prison', 'Severny'
            , 'Lipovka', 'Mylta Power', 'Primorsk', 'Shelter', 'Novorepnoye'
            , 'Water Park', 'Bridge'];
var place_c = ['Gatka', 'Ruins', 'Mansion', 'Quarry', 'Farm'
            , 'Ferry pier', 'Stalber', 'Kameshki', 'West_Coast'];

// Map Images
var ERANGEL = images("./Images/ERANGEL.png").size(2688, 2688);
var RECTANGLE = images("./Images/RECTANGLE.png").size(336, 336);

module.exports = class determinePlace extends Commando.Command{

    constructor(client){
        super(client, {
            name : 'map',
            group : 'map',
            memberName : 'map',
            description : 'determine falling place',
            examples : ['!map --- Choose one place',
            '!map list --- Show list',
            '!map a --- Choose one place within high dangerous zone.',
            '!map b --- Choose one place within noraml dangerous zone.',
            '!map c --- Choose one place within low dangerous zone.',
            '!map coor --- Select coordinates on the map.'],
            args : [
                {
                    key : 'map',
                    prompt : 'None',
                    type : 'string',
                    default : 'None'
                }
            ]
        });
    }

    async run(message, args){

        if(args.map == 'list'){
            message.reply('\n(a)위험도-상 : ' + place_a
                        + '\n\n(b)위험도-중 : ' + place_b
                        + '\n\n(c)위험도-하 : ' + place_c);
        }
        else if(args.map == 'a'){
            message.reply("상 : " + place_a[Math.floor(Math.random()*place_a.length)]);
        }
        else if(args.map == 'b'){
            message.reply("중 : " + place_b[Math.floor(Math.random()*place_b.length)]);
        }
        else if(args.map == 'c'){
            message.reply("하 : " + place_c[Math.floor(Math.random()*place_c.length)]);
        }
        else if(args.map == 'coor'){
            var map_row = ['A', 'J', 'K', 'L', 'M', 'N', 'O', 'P'];
            var map_col = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
            
            var temp_row = Math.floor(Math.random()*map_row.length);
            var temp_col = Math.floor(Math.random()*map_col.length);
            var temp_coor = map_row[temp_row].toString() + map_col[temp_col].toString();
            
            var except_coor = ['AA', 'AB', 'AC', 'AD', 'JH', 'KA', 'KH', 'LA', 'NH'
                            , 'OA', 'OC', 'OH', 'PA', 'PB', 'PC', 'PF', 'PG', 'PH'];

            function isContain(element, ary){
                for(var x in ary){
                    if(ary[x] == element) return true;
                }
                return false;
            }

            while(isContain(temp_coor, except_coor)){
                message.reply("Except : " + map_row[temp_row] + " - " + map_col[temp_col]);
                temp_row = Math.floor(Math.random()*map_row.length);
                temp_col = Math.floor(Math.random()*map_col.length);
                temp_coor = map_row[temp_row].toString() + map_col[temp_col].toString();
            }

            // message.reply("Coordinate : " + temp_row + " - " + temp_col);

            images(ERANGEL).draw(images(RECTANGLE), temp_col*336, temp_row*336).save("./Images/output.jpg");
            message.reply("Coordinate : " + map_row[temp_row] + " - " + map_col[temp_col], {file : "./Images/output.jpg"});
        }
        else{
            var place = place_a.concat(place_b).concat(place_c);
            message.reply("Overall : " + place[Math.floor(Math.random()*place.length)]);
        }
    }
}